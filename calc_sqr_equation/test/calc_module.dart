import 'dart:io';
import 'dart:math';

main(){
  double a,b,c;
  double D,x1,x2;
  a = double.parse(stdin.readLineSync());
  b = double.parse(stdin.readLineSync());
  c = double.parse(stdin.readLineSync());
  print("$a^2 +" + b.toString() + "x + $c");
  print("Найдём дискриминант: ");
  D = b*b + 4*a*c;
  if(D > 0){
    x1 = (-b + sqrt(D))/(2*a);
    x2 = (-b - sqrt(D))/(2*a);
    print("Ответ: \n x1 = $x1 \n x2 = $x2");
  }
  else{
    print("Корней нет!")
  }
}
